﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace wpf_printing;

public class RelayCommand : ICommand
{
    Action m_action;

    public event EventHandler? CanExecuteChanged;

    public RelayCommand(Action action)
    {
        m_action = action;
    }

    public bool CanExecute(object parameter)
    {
        return true;
    }

    public void Execute(object parameter)
    {
        m_action.Invoke();
    }
}
