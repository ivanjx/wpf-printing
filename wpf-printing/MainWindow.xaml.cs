﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Management;
using System.Windows;
using System.Windows.Input;

namespace wpf_printing;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window, INotifyPropertyChanged
{
    Poppler m_poppler;

    string[] m_printers;
    string? m_selectedPrinter;
    string[] m_printerPages;
    string? m_selectedPrinterPage;
    string[] m_imagePaths;
    int m_selectedImagePathIndex;

    public event PropertyChangedEventHandler? PropertyChanged;

    public string[] Printers
    {
        get => m_printers;
        set
        {
            m_printers = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(Printers)));
        }
    }

    public string? SelectedPrinter
    {
        get => m_selectedPrinter;
        set
        {
            m_selectedPrinter = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(SelectedPrinter)));
            LoadPrinterPages();
        }
    }

    public string[] PrinterPages
    {
        get => m_printerPages;
        set
        {
            m_printerPages = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(PrinterPages)));
        }
    }

    public string? SelectedPrinterPage
    {
        get => m_selectedPrinterPage;
        set
        {
            m_selectedPrinterPage = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(SelectedPrinterPage)));
            SetPrinterPage();
        }
    }

    public string[] ImagePaths
    {
        get => m_imagePaths;
        set
        {
            m_imagePaths = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(ImagePaths)));
        }
    }

    public int SelectedImagePathIndex
    {
        get => m_selectedImagePathIndex;
        set
        {
            m_selectedImagePathIndex = value;
            PropertyChanged?.Invoke(
                this,
                new PropertyChangedEventArgs(nameof(SelectedImagePathIndex)));
        }
    }

    public ICommand AddImagesCommand
    {
        get => new RelayCommand(AddImages);
    }

    public ICommand RemoveImageCommand
    {
        get => new RelayCommand(RemoveImage);
    }

    public ICommand PrintCommand
    {
        get => new RelayCommand(Print);
    }

    public ICommand PrintPdfCommand
    {
        get => new RelayCommand(PrintPdf);
    }

    public MainWindow()
    {
        InitializeComponent();
        DataContext = this;

        m_poppler = new Poppler();

        m_printers = Array.Empty<string>();
        m_printerPages = Array.Empty<string>();
        m_imagePaths = Array.Empty<string>();
        Loaded += HandleLoad;
    }

    void HandleLoad(object sender, RoutedEventArgs e)
    {
        LoadPrinters();
    }

    void LoadPrinters()
    {
        var printers = PrinterSettings.InstalledPrinters;

        if (printers == null)
        {
            return;
        }

        List<string> result = new List<string>();

        foreach (string printer in printers)
        {
            result.Add(printer);
        }

        Printers = result.ToArray();
    }

    PaperSize[] GetPaperSizes(string printerName)
    {
        using PrintDocument doc = new PrintDocument();
        doc.PrinterSettings.PrinterName = printerName;
        var sizes = doc.PrinterSettings.DefaultPageSettings.PrinterSettings.PaperSizes;
        List<PaperSize> result = new List<PaperSize>();

        foreach (PaperSize size in sizes)
        {
            result.Add(size);
        }

        return result.ToArray();
    }

    void LoadPrinterPages()
    {
        SelectedPrinterPage = null;
        PrinterPages = Array.Empty<string>();

        if (string.IsNullOrEmpty(SelectedPrinter))
        {
            return;
        }

        PrinterPages = GetPaperSizes(SelectedPrinter!)
            .Select(x => x.PaperName)
            .ToArray();
    }

    void SetPrinterPage()
    {
        if (SelectedPrinterPage == null)
        {
            return;
        }

        PaperSize? size = GetPaperSizes(SelectedPrinter!)
            .Where(x => x.PaperName == SelectedPrinterPage!)
            .FirstOrDefault();

        if (size == null)
        {
            return;
        }
    }

    void AddImages()
    {
        OpenFileDialog ofd = new OpenFileDialog();
        ofd.Multiselect = true;
        bool? success = ofd.ShowDialog();

        if (success != true)
        {
            return;
        }

        ImagePaths = ImagePaths
            .Concat(ofd.FileNames)
            .ToArray();
        SelectedImagePathIndex = -1;
    }

    void RemoveImage()
    {
        if (SelectedImagePathIndex == -1)
        {
            return;
        }

        List<string> result = new List<string>();

        for (int i = 0; i < ImagePaths.Length; ++i)
        {
            if (i == SelectedImagePathIndex)
            {
                continue;
            }

            result.Add(ImagePaths[i]);
        }

        ImagePaths = result.ToArray();
        SelectedImagePathIndex = -1;
    }

    string? GetPrinterStatus(string name)
    {
        string query = string.Format(
            "SELECT * from Win32_Printer WHERE Name = '{0}'",
            name);
        using ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
        using ManagementObjectCollection objs = searcher.Get();

        foreach (ManagementObject o in objs)
        {
            using ManagementObject obj = o;
            return obj.Properties["PrinterStatus"]?.Value.ToString();
        }

        return null;
    }

    void Print()
    {
        if (string.IsNullOrEmpty(SelectedPrinter))
        {
            MessageBox.Show(
                "No printer selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        if (string.IsNullOrEmpty(SelectedPrinterPage))
        {
            MessageBox.Show(
                "No printer page selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        if (ImagePaths.Length == 0)
        {
            MessageBox.Show(
                "No image selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        string? status = GetPrinterStatus(SelectedPrinter!);

        if (status != null &&
            status != "3" && // Idle.
            status != "4") // Printing.
        {
            MessageBox.Show(
                "Invalid printer status",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        PaperSize? size = GetPaperSizes(SelectedPrinter!)
            .Where(x => x.PaperName == SelectedPrinterPage!)
            .FirstOrDefault();

        if (size == null)
        {
            MessageBox.Show(
                "Unable to get paper size",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        using PrintDocument doc = new PrintDocument();
        doc.PrinterSettings.PrinterName = SelectedPrinter;
        doc.PrinterSettings.DefaultPageSettings.PaperSize = size;
        int printCounter = 0;
        doc.PrintPage += (_, e) =>
        {
            try
            {
                string imagePath = ImagePaths[printCounter];
                using Image image = Image.FromFile(
                    ImagePaths[printCounter]);
                var rect = new Rectangle(
                    e.PageBounds.Width / 2 - image.Width / 2,
                    e.PageBounds.Height / 2 - image.Height / 2,
                    image.Width,
                    image.Height);
                e.Graphics.DrawImage(image, rect);

                ++printCounter;
                e.HasMorePages = printCounter < ImagePaths.Length;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                e.HasMorePages = false;
            }
        };
        doc.Print();
    }

    void PrintPdf()
    {
        if (string.IsNullOrEmpty(SelectedPrinter))
        {
            MessageBox.Show(
                "No printer selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        if (string.IsNullOrEmpty(SelectedPrinterPage))
        {
            MessageBox.Show(
                "No printer page selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        if (ImagePaths.Length == 0 ||
            !ImagePaths[0].EndsWith(".pdf"))
        {
            MessageBox.Show(
                "No pdf file selected",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        string? status = GetPrinterStatus(SelectedPrinter!);

        if (status != null &&
            status != "3" && // Idle.
            status != "4") // Printing.
        {
            MessageBox.Show(
                "Invalid printer status",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        PaperSize? size = GetPaperSizes(SelectedPrinter!)
            .Where(x => x.PaperName == SelectedPrinterPage!)
            .FirstOrDefault();

        if (size == null)
        {
            MessageBox.Show(
                "Unable to get paper size",
                "Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
            return;
        }

        using IPopplerDocument popplerDoc = m_poppler.LoadDocument(
            File.ReadAllBytes(ImagePaths[0]));
        using IPopplerRenderer popplerRenderer = m_poppler.InitRenderer();
        int popplerDocPageCount = m_poppler.GetDocumentPageCount(popplerDoc);

        using PrintDocument doc = new PrintDocument();
        doc.PrinterSettings.PrinterName = SelectedPrinter;
        doc.PrinterSettings.DefaultPageSettings.PaperSize = size;
        int printCounter = 0;
        doc.PrintPage += (_, e) =>
        {
            try
            {
                using IPopplerImage popplerImage = m_poppler.RenderDocumentPage(
                    popplerDoc,
                    popplerRenderer,
                    printCounter);
                using Bitmap image = m_poppler.GetBitmap(popplerImage);
                var rect = new Rectangle(
                    System.Drawing.Point.Empty,
                    e.PageBounds.Size);
                e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bicubic;
                e.Graphics.DrawImage(image, rect);

                ++printCounter;
                e.HasMorePages = printCounter < popplerDocPageCount;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                e.HasMorePages = false;
            }
        };
        doc.Print();
    }
}
