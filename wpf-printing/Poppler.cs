﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace wpf_printing;

public interface IPopplerDocument : IDisposable
{
}

public interface IPopplerRenderer : IDisposable
{
}

public interface IPopplerImage : IDisposable
{
}

public class Poppler
{
    class PopplerDocument : IPopplerDocument
    {
        public nint Handle
        {
            get;
        }

        public PopplerDocument(nint handle)
        {
            Handle = handle;
        }

        public void Dispose()
        {
            PopplerNative.doc_free(Handle);
        }
    }

    class PopplerRenderer : IPopplerRenderer
    {
        public nint Handle
        {
            get;
        }

        public PopplerRenderer(nint handle)
        {
            Handle = handle;
        }

        public void Dispose()
        {
            PopplerNative.renderer_free(Handle);
        }
    }

    class PopplerImage : IPopplerImage
    {
        public nint Handle
        {
            get;
        }

        public PopplerImage(nint handle)
        {
            Handle = handle;
        }

        public void Dispose()
        {
            PopplerNative.image_free(Handle);
        }
    }

    const int RGB_SIZE = 3;

    public IPopplerDocument LoadDocument(byte[] pdfData)
    {
        nint doc = PopplerNative.doc_load(pdfData, pdfData.Length);
        return new PopplerDocument(doc);
    }

    public int GetDocumentPageCount(IPopplerDocument doc)
    {
        PopplerDocument docImpl = (PopplerDocument)doc;
        return PopplerNative.doc_get_pages(docImpl.Handle);
    }

    public IPopplerRenderer InitRenderer()
    {
        nint renderer = PopplerNative.renderer_init();
        return new PopplerRenderer(renderer);
    }

    public IPopplerImage RenderDocumentPage(
        IPopplerDocument doc,
        IPopplerRenderer renderer,
        int pageNum)
    {
        PopplerDocument docImpl = (PopplerDocument)doc;
        PopplerRenderer rendererImple = (PopplerRenderer)renderer;
        nint image = PopplerNative.renderer_render_page(
            docImpl.Handle,
            rendererImple.Handle,
            pageNum);
        return new PopplerImage(image);
    }

    public Bitmap GetBitmap(IPopplerImage image)
    {
        nint handle = ((PopplerImage)image).Handle;
        int width = PopplerNative.image_get_width(handle);
        int height = PopplerNative.image_get_height(handle);
        byte[] buff = new byte[width * height * RGB_SIZE];
        PopplerNative.image_copy(
            handle,
            buff,
            buff.Length);
        Bitmap bitmap = new Bitmap(
            width,
            height);
        
        int i = 0;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                Color color = Color.FromArgb(
                    buff[i],
                    buff[i + 1],
                    buff[i + 2]);
                bitmap.SetPixel(x, y, color);
                i += RGB_SIZE;
            }
        }

        return bitmap;
    }
}

public static class PopplerNative
{
    const string POPPLER_WRAPPER = "PopplerWrapper.dll";

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint doc_load(
        byte[] data,
        int dataLen);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int doc_get_pages(
        nint doc);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void doc_free(
        nint doc);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint renderer_init();

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern nint renderer_render_page(
        nint doc,
        nint renderer,
        int pageNum);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int renderer_free(
        nint renderer);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int image_get_width(
        nint image);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern int image_get_height(
        nint image);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void image_copy(
        nint image,
        byte[] data,
        int dataLen);

    [DllImport(POPPLER_WRAPPER, CallingConvention = CallingConvention.Cdecl)]
    public static extern void image_free(
        nint image);
}
