#include <poppler/cpp/poppler-document.h>
#include <poppler/cpp/poppler-page-renderer.h>
#include <poppler/cpp/poppler-page.h>
#include <poppler/cpp/poppler-image.h>

#define EXTERN extern "C" __declspec(dllexport)

EXTERN poppler::document *doc_load(
    const char *data,
    unsigned int data_len)
{
    poppler::document *doc =
        poppler::document::load_from_raw_data(
            data,
            data_len);
    
    if (doc == nullptr)
    {
        return NULL;
    }

    return doc;
}

EXTERN int doc_get_pages(poppler::document *doc)
{
    return doc->pages();
}

EXTERN void doc_free(poppler::document* doc)
{
    delete doc;
}

EXTERN poppler::page_renderer *renderer_init()
{
    poppler::page_renderer *renderer =
        new poppler::page_renderer();
    
    if (renderer == nullptr)
    {
        return NULL;
    }

    renderer->set_render_hint(
        poppler::page_renderer::text_antialiasing);
    renderer->set_image_format(
        poppler::image::format_enum::format_rgb24);
    return renderer;
}

EXTERN poppler::image *renderer_render_page(
    poppler::document *doc,
    poppler::page_renderer *renderer,
    int page_num)
{
    poppler::page *page = doc->create_page(page_num);
    
    if (doc == nullptr)
    {
        return NULL;
    }

    poppler::image page_image = renderer->render_page(
        page,
        150,
        150);
    delete page;
    return new poppler::image(page_image);
}

EXTERN void renderer_free(poppler::page_renderer* renderer)
{
    delete renderer;
}

EXTERN int image_get_width(poppler::image *image)
{
    return image->width();
}

EXTERN int image_get_height(poppler::image* image)
{
    return image->height();
}

EXTERN void image_copy(
    poppler::image* image,
    char *out,
    int out_len)
{
    memcpy(
        out,
        image->const_data(),
        out_len);
}

EXTERN void image_free(poppler::image* image)
{
    delete image;
}
